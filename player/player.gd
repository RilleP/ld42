extends Node2D

var x = 0
var y = 0

onready var tilemap = get_node("../TileMap")
onready var tile_size = tilemap.cell_size.x





var items = []

var level_completed = false

var memories

func _ready():
	memories = get_node("../Memories")
	
	x = int(round(position.x / tile_size))
	y = int(round(position.y / tile_size))
	update_position()
	
func update_position():
	position.x = x*tile_size
	position.y = y*tile_size
	
	
func find_item_with_tag(tag):
	for item in items:
		if item.tag == tag:
			return item
	return null
	
func find_key_with_color(color):
	for item in items:
		if item.tag == "key" and item.color == color:
			return item
	return null
			
func has_item_with_tag(tag):
	return find_item_with_tag(tag) != null
	
func move_input(dx, dy):
	if !can_add_mem():
		add_feedback_text("Memory is full", Color(0.8, 0, 1))
		play_audio(preload("cant_move.wav"))
		return
	if move(dx, dy, true):
		update_rotation(dx, dy)
		

func add_feedback_text(text, color = Color(0.8, 0, 0)):
	var fb = preload("res://UI/feedback_text.tscn").instance()
	get_node("../UI").add_child(fb)
	fb.text = text
	fb.rect_position = position
	fb.add_color_override("font_color", color)

func can_add_mem():
	return memories.selected_input_list.can_add()

func set_level_completed():
	level_completed = true
	var window = get_node("../UI/LevelCompleted")
	window.visible = true
	window.raise()
	play_audio(preload("level_done.wav"))

func move(dx, dy, add_input = false):
	var nx = x + dx
	var ny = y + dy
	
	
	var can_move = can_move_to(nx, ny)
	if !can_move:
		add_feedback_text("Move blocked")
		play_audio(preload("cant_move.wav"))
		return false
	
	#var itemMemInput = null
	#var itemMemDesc = null
	var itemTexture = null
	var state_change
	if true or add_input:
		var item = tilemap.get_item_at_position(nx, ny)
		
		
		if item != null:
			if item.tag == "door":
				var key = find_key_with_color(item.color);
				if key != null:
					item.enabled = false
					items.erase(key)
					state_change = {"type": "open", "door": item, "key": key}
					#itemMemDesc = "open door"
					itemTexture = preload("res://door/door.png")
					play_audio(preload("res://door/door_open.wav"))
				else:
					add_feedback_text("Door is locked", item.get_node("Sprite").modulate);
					play_audio(preload("cant_move.wav"))
			if item.tag == "goal":
				set_level_completed()
			if item.blocks_movement():
				return false
			if item.can_be_picked_up():
				state_change = {"type": "pickup", "item": item}
				play_audio(preload("res://items/key/key_pickup.wav"))
				#itemMemDesc = "pickup %s" % item.name
				itemTexture = preload("res://items/key/key.png")
				items.append(item)
				item.enabled = false
			
	
	if can_move:
		x = nx
		y = ny
		update_position()
		if add_input:
			var input = {"type": "move", "dx": dx, "dy": dy, "is_input": true, "state_change": state_change}
			var desc = "move %s" % [dir_to_string(dx, dy)]
			memories.selected_input_list.add_input(input, desc, itemTexture)
			play_steps()
			#if itemMemInput != null:
			#	memories.selected_input_list.add_input(itemMemInput, itemMemDesc)
		return true
	return false
		
func play_steps():
	var steps = [
		preload("res://player/steps01.wav"),
		preload("res://player/steps02.wav"),
		preload("res://player/steps03.wav")
	]
	$StepsAudioPlayer.stream = steps[randi()%steps.size()]
	$StepsAudioPlayer.play()

		
func play_audio(stream):
	$AudioStreamPlayer.stream = stream
	$AudioStreamPlayer.play()
		
func dir_to_string(dx, dy):
	if dx > 0:
		return "RIGHT"
	if dx < 0:
		return "LEFT"
	if dy > 0:
		return "DOWN"
	if dy < 0:
		return "UP"
	
		
func update_rotation(dx, dy):
	var angle = atan2(dy, dx)
	get_node("Sprite").rotation = angle
		
func undo():
	var mem = memories.selected_input_list.peek_input()
	if mem == null:
		return
	if mem.type == "move":
		if move(-mem.dx, -mem.dy):
			update_rotation(mem.dx, mem.dy)
			memories.selected_input_list.pop_input()
			play_steps()
		else:
			return
		
	
	if mem.has("state_change"):
		var state_change = mem.state_change
		if state_change != null:		
			if state_change.type == "pickup":
				state_change.item.enabled = true
				items.erase(state_change.item)
				play_audio(preload("res://items/key/key_drop.wav"))
			elif state_change.type == "open":
				state_change.door.enabled = true
				items.append(state_change.key)
				play_audio(preload("res://door/door_close.wav"))
			
	if !mem.has("is_input") or mem["is_input"] == false:
		undo()
		
	
	
func can_move_to(x, y):
	var cell = tilemap.get_cell(x, y)
	return cell == -1


func _input(event):
	if level_completed:
		return
	if event is InputEventKey:
		if event.pressed:# and !event.is_echo():
			if event.scancode == KEY_DOWN:
				move_input(0, 1)
			if event.scancode == KEY_UP:
				move_input(0, -1)
			if event.scancode == KEY_LEFT:
				move_input(-1, 0)
			if event.scancode == KEY_RIGHT:
				move_input(1, 0)
			if event.scancode == KEY_Z and (event.control or event.command):
				undo()
		if event.pressed and !event.is_echo():
			if event.scancode == KEY_1:
				memories.select_input_list(0)
			if event.scancode == KEY_2:
				memories.select_input_list(1)
			if event.scancode == KEY_3:
				memories.select_input_list(2)
