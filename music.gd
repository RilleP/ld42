extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var player = AudioStreamPlayer.new()
	add_child(player)
	player.stream = preload("res://music/loop.wav")
	
	player.play()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
