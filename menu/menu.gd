extends Node

func _ready():
	$guy/AnimationPlayer.play("dance")

func _on_Button_pressed():
	get_tree().change_scene("res://Level/levels/learn1.tscn")

func _input(event):
	if event is InputEventKey:
		if event.pressed and !event.is_echo():
			if event.scancode == KEY_ENTER:
				_on_Button_pressed()
			if event.scancode == KEY_ESCAPE:
				get_tree().quit()