extends ItemList

onready var space_label = get_node("SpaceLabel");

var ram = []
var ram_size = 12

var selected = false setget set_selected

func set_selected(new_value):
	selected = new_value
	if selected:
		grab_focus()


func can_add():
	return ram.size() < ram_size

func add_input(input, desc, texture = null):
	assert(can_add())
	ram.append(input)
	#add_item(desc);
	add_item(desc, texture, false);
	update_space_label();
	
func peek_input():
	if ram.empty():
		return null
		
	var input = ram[get_item_count()-1]
	return input

func pop_input():
	if ram.empty():
		return null
		
		
	var input = ram.pop_back()
	remove_item(get_item_count()-1)
	update_space_label();
	return input
	
func update_space_label():
	space_label.text = "Space: %d/%d" % [ram_size - ram.size(), ram_size]

func _on_InputList_focus_entered():
	get_parent().selected_input_list = self
