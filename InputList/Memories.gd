extends Control

onready var input_lists = []
var selected_input_list setget set_selected_input_list

func set_selected_input_list(new_value):
	if selected_input_list != null:
		selected_input_list.selected = false
	selected_input_list = new_value
	selected_input_list.selected = true

func _ready():
	for i in range(get_child_count()):
		input_lists.append(get_child(i))
		get_child(i).get_node("NameLabel").text = "Memory Slot %d" % (i+1)
		
	self.selected_input_list = input_lists[0]

func select_input_list(index):
	if input_lists.size() > index:
		self.selected_input_list = input_lists[index]