extends "res://items/item.gd"

tool

export var color = "gold" setget set_color

func set_color(new_value):
	color = new_value
	set_visual_color(color)


func _ready():
	tag = "key"
	$AnimationPlayer.play("key_rotate")
	
func can_be_picked_up():
	return true
	
func blocks_movement():
	return false