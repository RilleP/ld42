extends Node2D


var x = 0
var y = 0

const goldColor = Color(.98, .83, 0)
const silverColor = Color(1, 1, 1)
const purpleColor = Color(0.7, 0.1, 1)

onready var tilemap = get_node("../TileMap")
onready var tile_size = tilemap.cell_size.x

var tag = "item"

func set_visual_color(color):
	match color:
		"gold":
			$Sprite.modulate = goldColor
		"silver":
			$Sprite.modulate = silverColor
		"purple":
			$Sprite.modulate = purpleColor

var enabled = true setget set_enabled

func set_enabled(new_value):
	if enabled == new_value:
		return
		
	enabled = new_value
	visible = enabled
	

func can_be_picked_up():
	return false
	
func blocks_movement():
	return false

func _ready():
	x = int(round(position.x / tile_size))
	y = int(round(position.y / tile_size))
	update_position()
	
	tilemap.items.append(self)
	
func update_position():
	position.x = x*tile_size
	position.y = y*tile_size

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
