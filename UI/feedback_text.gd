extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var velocity

var time_remaining = 1.5

func _ready():
	var direction = Vector2(randf()*2.0 - 1.0, randf()*2.0 - 1.0)
	velocity = direction * 50.0

func _process(delta):
	rect_position += velocity*delta	
	time_remaining -= delta
	self_modulate.a = time_remaining
	if time_remaining <= 0.0:
		queue_free()
