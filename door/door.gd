extends "res://items/item.gd"

tool

export var color = "gold" setget set_color
export(bool) var horizontal = false setget set_horizontal

func set_horizontal(new_value):
	horizontal = new_value
	var s = get_node("BgSprite")
	if s != null:
		if horizontal:
			s.rotation = PI/2
		else:
			s.rotation = 0
	
func set_color(new_value):
	color = new_value
	set_visual_color(color)

func set_enabled(new_value):
	if new_value == enabled:
		return
	enabled = new_value
	
	if !enabled:
		$AnimationPlayer.play("open")
	else:
		$AnimationPlayer.play("open", -1, -1, true);

func _ready():
	tag = "door"
	
func can_be_picked_up():
	return false
	
func blocks_movement():
	return enabled