extends "res://items/item.gd"

func _ready():
	tag = "goal"
	$AnimationPlayer.play("shine")
	
func can_be_picked_up():
	return false
	
func blocks_movement():
	return false