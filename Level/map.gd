extends TileMap

var items = []

func get_item_at_position(x, y):
	for item in items:
		if item.enabled and item.x == x and item.y == y:
			return item
	return null
	