extends Panel

var levels = [
	"learn1",
	"learn2",
	"learn3",
	"level1",
	"level2",
	"level3",
	"level4",
	"level5",
	"level6",
	"level7",
	"level8",
	"level9",
]

var current_level_index = 0

func _ready():
	current_level_index = get_current_level_index()
	
	if current_level_index == levels.size()-1:
		get_node("NextLevelButton").text = "QUIT"
		get_node("Label").text = "All levels Completed!"
	
	
func get_current_level_index():
	var filepath = get_tree().current_scene.filename
	var level_name = filepath.get_file()
	level_name = level_name.left(level_name.length() - 5)
	var level_index = levels.find(level_name)
	return level_index;

func _on_NextLevelButton_pressed():
	current_level_index = get_current_level_index();
	if current_level_index == levels.size()-1:
		get_tree().quit()
		return
		
	load_level_index(current_level_index+1)

func _on_RestartButton_pressed():
	get_tree().reload_current_scene()
	
func load_level_index(index):
	if index < 0:
		index = 0
	var level_name = levels[index%levels.size()]
	load_level_name(level_name)

func get_level_file_path(level_name):
	return "res://Level/levels/%s.tscn" % level_name
	
func load_level_name(level_name):
	var path = get_level_file_path(level_name)
	get_tree().change_scene(path)

	
func load_level(level):
	queue_free()
	
	get_node("/root").add_child(level)

func _input(event):
	if event is InputEventKey:
		if event.pressed and !event.is_echo():
			if event.scancode == KEY_P:
				load_level_index(5)
				
			if event.scancode == KEY_ENTER and visible:
				_on_NextLevelButton_pressed()
			if event.scancode == KEY_R:
				_on_RestartButton_pressed()
			if event.scancode == KEY_ESCAPE:
				get_tree().quit()